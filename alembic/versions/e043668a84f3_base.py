"""base

Revision ID: e043668a84f3
Revises: 
Create Date: 2022-07-24 20:12:57.832196

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e043668a84f3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        'board-manufacturer',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('nome', sa.String(length=50), nullable=False)
    )


def downgrade() -> None:
    op.drop_table(
        'board-manufacturer'
    )
